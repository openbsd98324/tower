# tower


# Overview 

v5: Cool tower

v6: Revemp-1 

v7: Cool mini, small sized. 


# V7 


> renkforce-amd64


LIST 
````
1.) Renkforce PC tuning kit AMD Ryzen III 3200G 8GB AMD Radeon Vega Graphics Vega
    Renkforce PC tuning kit AMD Ryzen™ 3 3200G (4 x 3.6 GHz) 8 GB AMD Radeon Vega Graphics Vega 8 Micro-ATX
    It integrates an asus-B450M-S2H.

2.) KOLINK Core 600w, atx series power supply KL-C600   

3.) Corsair K55 gaming RGP0031 keyboard

4.) LED 27 inch ls27f358led samsung 27inch

5.) Tower Aero Cool  
    aerocool star mini micro - atx gehause -   
````


![](pub/manual/v7-ryzen-3/20220918-112846.jpg)

![](pub/manual/v7-ryzen-3/20220918-112907-rotated.jpg)

![](pub/manual/v7-ryzen-3/1668335966-1-pcumr.png)
![](pub/manual/v7-ryzen-3/1668335966-2-rgbk55.png)
![](pub/manual/v7-ryzen-3/1668335967-3-ls27f358led-samsung-27inch.png)
![](pub/manual/v7-ryzen-3/1668335967-4-mb_manual_b450m-s2h-v2_e_v1.pdf)
![](pub/manual/v7-ryzen-3/20220918-112846.jpg)
![](pub/manual/v7-ryzen-3/20220918-112907-rotated.jpg)


![](pub/manual/v7-ryzen-3/1668336259-1-aerocool-mini.png) 


````
RENKFORCE PC TUNING KIT AMD RYZEN 3 3200 G8G

Renkforce upgrade kit to get the complete package in your computer. The tuning kit is one of the most effective ways to achieve the targeted performance enhancement of a desktop computer. The perfectly coordinated complete set consists of mainboard, processor, RAM and CPU cooler and is already assembled. Replacing key components that are critical to the performance of the PC will bring the PC up to date with state-of-the-art technology.

Perfectly matched 

CPU: AMD Ryzen3 3200G (3.6 to 4.0 GHZ) incl. branded CPU cooler

Main memory: 8GB DDR4 

Graphics: Radeon Vega 8 (on board)

Fully assembled and tested!

Even more power 

Upgrading your PC system with a new tuning kit offers you the perfect option to replace additional hardware with the latest components. You decide yourself whether existing drives are still used or whether your system is supplemented by a fast SSD, for example. Replace your PC case and power supply that has been coming into the years and put on a new design and higher energy efficiency. With existing plug-in cards such as graphics cards, make sure that the new upgrade set provides the corresponding interfaces (PCI / PCIe x1 / PCIe x16).

Highlights & Details

AMD Ryzen™ 3 3200G (4 x 3.6 GHz) CPU

Micro-ATX motherboard.

The ultimate performance booster for your PC


Range of uses	Office, Design, Home, Media	

Chipset	AMD® B450	

Form factor	Micro-ATX	

RAM	8 GB	

Graphics card type	AMD Radeon Vega Graphics	
Graphics card type	Vega 8	
Motherboard	Gigabyte B450M S2H	
CPU series	AMD Ryzen™ 3	

Processor/type	3200G	

Processor cores (No.)	4 x	
CPU speed	3.6 GHz	
CPU turbo clock speed (max.)	4 GHz	
No. of DDR4 slots	2 x	
No. of Gigabit LANs	1 x	
No. of PCIe x1	2 x	
No. of PCIe 3.0 x16	1 x	
No. of PS/2 ports	2 x	
No. of SATA III ports	4 x	
No. of USB 2.0 ports	2 x	
Onboard graphics	Radeon Vega 8	
Onboard audio	5.1	
Base	AMD AM4	

No. of USB 3.2 2nd Gen (USB 3.1)	4 x	
Product type	PC tuning kit	


Non-ECC unbuffered DDR4 dual channel, 2 DIMMs
HDMI, DVI-D, D-sub ports for multiple display
Ultra fast PCIe Gen3 PCIe x4 M.2 mit NVMe & SATA mode support
GIGABYTE Exclusive8118 Gaming LAN mit Bandwidth Management
RGB Fusion Support RGB LED strips in 7 colors
High-quality audio capacitors and audio noise guard design
Smart Fan 5 features 5 Temperature sensor and 2 Hybrid fan header
APP Center, including easy tune™ and Cloud Station™ Utilities
````



